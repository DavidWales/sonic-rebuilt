﻿using System.Collections;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour
{

    public float Seconds;
    public bool ObjectPoolDestruction = true;

    private Coroutine _timeout;

    private void OnEnable()
    {
        _timeout = StartCoroutine(Destroy());
    }

    private void OnDisable()
    {
        if (_timeout != null) StopCoroutine(_timeout);
    }

    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(Seconds);
        if (ObjectPoolDestruction) ObjectPoolManager.Destroy(gameObject);
        else Destroy(gameObject);
    }

}
