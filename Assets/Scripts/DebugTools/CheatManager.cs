﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheatManager : MonoBehaviour
{

    public GameObject ExplosionPrefab;
    public List<DialogueModel> Dialogue;
    public GameObject Player;

    private Dictionary<KeyCode, Action> _cheatActions;
    private DamageCommand _damagePlayerCommand;

    private void Awake()
    {
        _cheatActions = new Dictionary<KeyCode, Action>
        {
            { KeyCode.F1, () => ExplodePlayer() },
            { KeyCode.F2, () => ShowDialogue() },
            { KeyCode.F3, () => DamagePlayer() },
            { KeyCode.F4, () => NextWave() }
        };
        _damagePlayerCommand = new DamageCommand(new DamageCommandParameter
        {
            Damage = 10,
            Target = Player,
            Source = gameObject
        });
    }

    private void Update()
    {
        foreach(var action in _cheatActions)
        {
            if (Input.GetKeyDown(action.Key)) action.Value();
        }
    }

    private void ExplodePlayer()
    {
        var explodeCommand = new ExplodeCommand(new ExplodeCommandParameter
        {
            ShrinkSpeed = 10f,
            Recycle = false,
            Source = GameObject.FindGameObjectWithTag("Player")
        });
        explodeCommand.Execute();
    }

    private void ShowDialogue()
    {
        foreach(var line in Dialogue)
        {
            DialogueController.QueueDialogue(line);
        }
    }

    private void DamagePlayer()
    {
        _damagePlayerCommand.Execute();
    }

    private void NextWave()
    {
        var waveController = GameObject.FindObjectOfType<WaveController>();
        var wave = waveController.Waves.First();
        waveController.StopAllCoroutines();
        wave.Enemies.RemoveRange(0, wave.Enemies.Count);
        waveController.Handle(wave);

        var enemies = GameObject.FindObjectsOfType<BaseEnemy>();
        foreach(var e in enemies)
        {
            var healthComponent = e.GetComponent<HealthComponent>();
            var damageCommand = new DamageCommand(new DamageCommandParameter
            {
                Damage = healthComponent.CurrentHealth,
                Source = gameObject,
                Target = e.gameObject
            });
            damageCommand.Execute();
        }
    }

}
