﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : MonoBehaviour
{

    public GameObject Target;
    public float VerticalPaddingPercent = 0.1f;
    public float HorizontalPaddingPercent = 0.2f;
    public bool TrackVertical = true;
    public bool TrackHorizontal = true;

    private Camera _camera;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    private void Update()
    {
        MoveWithTarget();
    }

    private void MoveWithTarget()
    {
        if (Target == null) return;
        var targetScreenPosition = _camera.WorldToScreenPoint(Target.transform.position);
        HandleHorizontal(targetScreenPosition);
        HandleVertical(targetScreenPosition);
    }

    private void HandleVertical(Vector2 targetScreenPosition)
    {
        if (!TrackVertical) return;

        var percent = targetScreenPosition.y / Screen.height;
        var offset = 0f;

        if (percent < VerticalPaddingPercent)
        {
            //Bound bottom
            var boundary = GetBoundary(Vector3.up, VerticalPaddingPercent, Screen.height).y;
            offset = Target.transform.position.y - boundary;
        }
        else if (percent > 1f - VerticalPaddingPercent)
        {
            //Bound top
            var boundary = GetBoundary(Vector3.up, 1f - VerticalPaddingPercent, Screen.height).y;
            offset = Target.transform.position.y - boundary;
        }

        _camera.transform.position += Vector3.up * offset;
    }

    private void HandleHorizontal(Vector2 targetScreenPosition)
    {
        if (!TrackHorizontal) return;

        var percent = targetScreenPosition.x / Screen.width;
        var offset = 0f;

        if(percent < HorizontalPaddingPercent)
        {
            //Bound left
            var boundary = GetBoundary(Vector3.right, HorizontalPaddingPercent, Screen.width).x;
            offset = Target.transform.position.x - boundary;
        }
        else if(percent > 1f - HorizontalPaddingPercent)
        {
            //Bound right
            var boundary = GetBoundary(Vector3.right, 1f - HorizontalPaddingPercent, Screen.width).x;
            offset = Target.transform.position.x - boundary;
        }

        _camera.transform.position += Vector3.right * offset;
    }

    private Vector3 GetBoundary(Vector3 dir, float padding, int screenSize)
    {
        var boundary = dir * (padding * screenSize);
        return _camera.ScreenToWorldPoint(boundary);
    }

}
