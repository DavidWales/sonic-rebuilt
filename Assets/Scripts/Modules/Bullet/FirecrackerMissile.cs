﻿public class FirecrackerMissile : Bullet
{

    public BulletExplosionCommandParameter BulletExplodeCommandParameter;
    public ExplodeCommandParameter ExplodeCommandParameter;

    private BulletExplosionCommand _bulletExplodeCommand;
    private ExplodeCommand _explodeCommand;

    protected override void Awake()
    {
        base.Awake();

        BulletExplodeCommandParameter.Source = gameObject;
        _bulletExplodeCommand = new BulletExplosionCommand(BulletExplodeCommandParameter);

        ExplodeCommandParameter.Source = gameObject;
        _explodeCommand = new ExplodeCommand(ExplodeCommandParameter);
    }

    protected override void OnRecycle()
    {
        _explodeCommand.Execute();
        _bulletExplodeCommand.Execute();
        base.OnRecycle();
    }

}
