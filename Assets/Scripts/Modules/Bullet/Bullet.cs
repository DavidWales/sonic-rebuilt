﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class Bullet : ObserverSubjectBehaviour<Bullet>
{

    public DamageCommandParameter DamageCommandParameter;
    public float Duration = 5f;

    private Coroutine _bulletTimeout;
    private GameObject _ignoredColliderObject;
    private DamageCommand _damageCommand;

    protected virtual void Awake()
    {
        DamageCommandParameter.Source = gameObject;
        _damageCommand = new DamageCommand(DamageCommandParameter);
    }

    public virtual void Initialize(GameObject source, Dictionary<string, object> initializationParameters)
    {
        _bulletTimeout = StartCoroutine(Recycle());
        SetIgnoreSourceCollision(source, ignore: true);
    }

    private void SetIgnoreSourceCollision(GameObject source, bool ignore)
    {
        if (!TryGetComponent(out Collider2D bulletCollider)) return;
        if (source == null || !source.TryGetComponent(out Collider2D sourceCollider)) return;
        Physics2D.IgnoreCollision(bulletCollider, sourceCollider, ignore);
        _ignoredColliderObject = source;
    }

    private void OnDisable()
    {
        if (_bulletTimeout != null) StopCoroutine(_bulletTimeout);
    }

    protected virtual void OnRecycle()
    {

    }

    private IEnumerator Recycle()
    {
        yield return new WaitForSeconds(Duration);
        OnRecycle();
        ObjectPoolManager.Destroy(gameObject);
        SetIgnoreSourceCollision(_ignoredColliderObject, ignore: false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ObjectPoolManager.Destroy(gameObject);
        DamageCommandParameter.Target = collision.gameObject;
        _damageCommand.Execute();
    }

}
