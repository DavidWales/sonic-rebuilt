﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HomingMissile : Bullet
{

    public GameObject Target;
    public ExplodeCommandParameter ExplodeCommandParameter;

    private ExplodeCommand _explodeCommand;
    private Rigidbody2D _rigidBody;

    protected override void Awake()
    {
        base.Awake();
        ExplodeCommandParameter.Source = gameObject;
        _explodeCommand = new ExplodeCommand(ExplodeCommandParameter);
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    public override void Initialize(GameObject source, Dictionary<string, object> initializationParameters)
    {
        base.Initialize(source, initializationParameters);
        
        if(initializationParameters.TryGetValue("Target", out object targetObj))
        {
            Target = targetObj as GameObject;
        }
    }

    private void Update()
    {
        if (Target == null) return;
        var dir = Target.transform.position - transform.position;
        dir.z = 0f;
        dir.Normalize();
        transform.right = dir;
        _rigidBody.velocity = dir * _rigidBody.velocity.magnitude;
    }

    protected override void OnRecycle()
    {
        base.OnRecycle();
        _explodeCommand.Execute();
    }

}
