﻿using System;
using UnityEngine;

public class BackgroundEntity : MonoBehaviour
{

    public SpriteRenderer SpriteRenderer;
    public Action<BackgroundEntity> Callback;
    private bool _wasVisible;

    private void OnBecameVisible()
    {
        _wasVisible = true;
    }

    private void OnBecameInvisible()
    {
        if (!_wasVisible) return;
        Callback(this);
    }

}
