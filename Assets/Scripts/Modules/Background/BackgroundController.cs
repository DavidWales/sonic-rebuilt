﻿using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{

    public GameObject BackgroundPrefab;
    public float BackgroundSpeed = 1f;
    public Vector3 BackgroundMovementDirection = Vector3.left;

    private List<BackgroundEntity> _backgrounds;
    private float _xOffset;

    private void Awake()
    {
        InitializeBackgrounds();
    }

    private void Update()
    {
        for(int i = 0; i < _backgrounds.Count; i++)
        {
            MoveBackground(_backgrounds[i], isFirst: i == 0);
        }
    }

    private void MoveBackground(BackgroundEntity background, bool isFirst = false)
    {
        background.transform.position += BackgroundMovementDirection * BackgroundSpeed * Time.deltaTime;
    }

    private void InitializeBackgrounds()
    {
        _backgrounds = new List<BackgroundEntity>();
        var firstBackground = CreateBackground();
        var secondBackground = CreateBackground();
        _xOffset = firstBackground.SpriteRenderer.sprite.bounds.extents.x * 2f;
        secondBackground.transform.position = firstBackground.transform.position + (Vector3.right * _xOffset);
    }

    private BackgroundEntity CreateBackground()
    {
        var background = GameObject.Instantiate(BackgroundPrefab, Vector3.zero, Quaternion.identity, transform);
        var bgEntity = background.AddComponent<BackgroundEntity>();
        bgEntity.SpriteRenderer = background.GetComponent<SpriteRenderer>();
        bgEntity.Callback = LoopBackground;
        _backgrounds.Add(bgEntity);
        return bgEntity;
    }

    private void LoopBackground(BackgroundEntity background)
    {
        var lastBackground = _backgrounds[_backgrounds.Count - 1];
        background.transform.position = lastBackground.transform.position + (Vector3.right * _xOffset);
        _backgrounds.RemoveAt(0);
        _backgrounds.Add(background);
    }

}
