﻿using UnityEngine;

public class BulletCone : Ability
{

    public BulletConeCommandParameter BulletConeCommandParameter;
    private BulletConeCommand _bulletConeCommand;

    protected override void Awake()
    {
        base.Awake();
        BulletConeCommandParameter.Source = gameObject;
        _bulletConeCommand = new BulletConeCommand(BulletConeCommandParameter);
    }

    protected override void CastAbility()
    {
        if (Target == null) return;
        var dir = Target.transform.position.x > transform.position.x
            ? Vector2.right
            : Vector2.left;
        BulletConeCommandParameter.Direction = dir;
        _bulletConeCommand.Execute();
    }

}
