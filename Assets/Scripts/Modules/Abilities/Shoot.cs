﻿using System.Collections.Generic;

public class Shoot : Ability
{

    public ShootCommandParameter ShootCommandParameter;
    private ShootCommand _shootCommand;

    protected override void Awake()
    {
        base.Awake();
        ShootCommandParameter.Source = gameObject;
        _shootCommand = new ShootCommand(ShootCommandParameter);
    }

    protected override void CastAbility()
    {
        ShootCommandParameter.InitializationParameters = new Dictionary<string, object>
        {
            { "Target", Target }
        };
        _shootCommand.Execute();
    }

}
