﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ChainAbilityCaster : MonoBehaviour
{

    public GameObject Target;
    private IEnumerable<Ability> _abilities;

    private void Awake()
    {
        Target = GameObject.FindGameObjectWithTag("Player");
        _abilities = GetComponents<Ability>();
        if (_abilities == null) return;
        _abilities = _abilities.OrderBy(a => a.Rank);
    }

    public void SetActive(bool isActive)
    {
        StopAllCoroutines();
        if (isActive)
        {
            StartCoroutine(ChainAbilities());
        }
    }

    private IEnumerator ChainAbilities()
    {
        var ability = _abilities.FirstOrDefault(a => !a.OnCooldown);
        if(ability == null)
        {
            //All abilities are on cd, try next frame
            yield return 0;
            StartCoroutine(ChainAbilities());
            yield break;
        }

        ability.Target = Target;    
        ability.Cast();
        yield return new WaitForSeconds(ability.Duration);
        StartCoroutine(ChainAbilities());
    }

}
