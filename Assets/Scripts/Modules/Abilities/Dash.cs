﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Dash : Ability
{

    public ApplyForceCommandParameter DashCommandParameter;
    private ApplyForceCommand _dashCommand;

    protected override void Awake()
    {
        base.Awake();
        var rigidBody = GetComponent<Rigidbody2D>();
        DashCommandParameter.Rigidbody = rigidBody;
        _dashCommand = new ApplyForceCommand(DashCommandParameter);
    }

    protected override void CastAbility()
    {
        if (Target == null) return;
        var dir = Target.transform.position.x > transform.position.x
            ? Vector2.right
            : Vector2.left;
        DashCommandParameter.Direction = (Vector2.up + dir).normalized;
        _dashCommand.Execute();
    }

}
