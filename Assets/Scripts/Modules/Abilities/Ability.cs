﻿using System.Collections;
using UnityEngine;

public abstract class Ability : MonoBehaviour
{

    public float Cooldown = 2f;
    public int Rank = 0;
    public float Duration = 1f;
    public GameObject Target = null;
    public Sprite AbilitySprite = null;
    public Sprite CompleteSprite = null;
    public bool OnCooldown { get; private set; }

    private SpriteRenderer _spriteRenderer;

    protected virtual void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    protected virtual void SetSprite(bool isComplete = false)
    {
        if (_spriteRenderer == null || AbilitySprite == null) return;
        _spriteRenderer.sprite = isComplete
            ? CompleteSprite
            : AbilitySprite;
    }

    public void Cast()
    {
        if (OnCooldown) return;
        StartCoroutine(HandleCooldown());
        CastAbility();
        SetSprite(isComplete: false);
    }

    protected abstract void CastAbility();

    protected virtual IEnumerator HandleCooldown()
    {
        OnCooldown = true;
        yield return new WaitForSeconds(Cooldown);
        OnCooldown = false;
    }

    protected virtual IEnumerable ResetSprite()
    {
        yield return new WaitForSeconds(Duration);
        SetSprite(isComplete: true);
    }

}
