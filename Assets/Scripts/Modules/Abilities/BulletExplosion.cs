﻿public class BulletExplosion : Ability
{

    public BulletExplosionCommandParameter BulletExplosionCommandParameter;
    private BulletExplosionCommand _bulletExplosionCommand;

    protected override void Awake()
    {
        base.Awake();
        BulletExplosionCommandParameter.Source = gameObject;
        _bulletExplosionCommand = new BulletExplosionCommand(BulletExplosionCommandParameter);
    }

    protected override void CastAbility()
    {
        _bulletExplosionCommand.Execute();
    }

}
