﻿public class HealthComponent : ObserverSubjectBehaviour<HealthComponent>
{

    public int MaxHealth;

    public int CurrentHealth;

    public bool Invincible = false;

}
