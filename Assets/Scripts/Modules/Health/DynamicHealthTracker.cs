﻿using Talos.Common.DesignPatterns.Observer;
using UnityEngine;
using UnityEngine.UI;

public class DynamicHealthTracker : MonoBehaviour, IObserver<HealthComponent>
{

    private SliderHealthTracker _trackerComponent;

    private void Start()
    {
        SetActive(false);
    }

    public void RegisterTrackedObject(GameObject trackedObject)
    {
        if (_trackerComponent != null) Destroy(_trackerComponent);

        var healthComponent = trackedObject.GetComponent<HealthComponent>();
        healthComponent.AttachObserver(this);

        _trackerComponent = gameObject.AddComponent<SliderHealthTracker>();
        _trackerComponent.GameObjectTag = "Boss";
        _trackerComponent.Target = trackedObject;
        SetActive(true);
    }

    public void Handle(HealthComponent subject)
    {
        if (subject.CurrentHealth > 0) return;
        SetActive(false);
        Destroy(_trackerComponent);
    }

    private void SetActive(bool isActive)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            child.gameObject.SetActive(isActive);
        }
    }

}
