﻿using Talos.Common.DesignPatterns.Observer;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderHealthTracker : MonoBehaviour, IObserver<HealthComponent>
{

    public GameObject Target;
    public string GameObjectTag = "Player";
    private Slider _slider;

    private void Awake()
    {
        _slider = GetComponent<Slider>();
    }

    private void Start()
    {
        Target = Target ?? GameObject.FindGameObjectWithTag(GameObjectTag);
        RegisterAsObserver(Target);
    }

    private void RegisterAsObserver(GameObject trackedObject)
    {
        if (trackedObject == null) return;
        if (!trackedObject.TryGetComponent(out HealthComponent trackedHealth)) return;
        trackedHealth.AttachObserver(this);
        Handle(trackedHealth); //Initialize bar
    }

    public void Handle(HealthComponent subject)
    {
        float trackedPercentHealth = subject.CurrentHealth / GetNonZeroValue(subject.MaxHealth);
        _slider.value = trackedPercentHealth * (_slider.maxValue - _slider.minValue) + _slider.minValue;
    }

    private float GetNonZeroValue(float value, float defaultValue = 1f)
    {
        if (value == 0) return defaultValue;
        return value;
    }

}
