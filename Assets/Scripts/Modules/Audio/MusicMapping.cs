﻿using System;
using UnityEngine;

[Serializable]
public class MusicMapping
{

    public Music Music;

    public AudioClip Clip;

}