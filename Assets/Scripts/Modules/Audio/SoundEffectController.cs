﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundEffectController : SingletonBehavior<SoundEffectController>
{

    public List<SoundEffectMapping> SoundEffects;
    private AudioSource _audioSource;

    protected override void Awake()
    {
        base.Awake();
        _audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(this);
    }

    public static void PlayerSoundEffect(SoundEffect soundEffect)
    {
        var instance = GetInstance();
        if (instance == null || instance._audioSource == null) return;

        var soundEffectMapping = instance.SoundEffects.FirstOrDefault(se => se.SoundEffect == soundEffect);
        if (soundEffectMapping == null) return;

        instance._audioSource.PlayOneShot(soundEffectMapping.Clip, soundEffectMapping.Volume);
    }

}
