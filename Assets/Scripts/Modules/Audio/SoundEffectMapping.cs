﻿using System;
using UnityEngine;

[Serializable]
public class SoundEffectMapping
{

    public SoundEffect SoundEffect;

    public AudioClip Clip;

    [Range(0f, 1f)]
    public float Volume = 1f;

}
