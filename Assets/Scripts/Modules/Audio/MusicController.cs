﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicController : SingletonBehavior<MusicController>
{
    public List<MusicMapping> MusicMappings;
    public Music DefaultMusic;

    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        PlayMusic(DefaultMusic);
    }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this);
    }

    public static void PlayMusic(Music music, bool loop = true)
    {
        var instance = GetInstance();
        var mapping = instance.MusicMappings.FirstOrDefault(mm => mm.Music == music);
        if (mapping == null || instance._audioSource.clip == mapping.Clip) return;
        instance._audioSource.clip = mapping.Clip;
        instance._audioSource.loop = loop;
        instance._audioSource.Play();
    }

}
