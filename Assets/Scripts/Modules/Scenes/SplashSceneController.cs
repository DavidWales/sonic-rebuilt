﻿using UnityEngine;

public class SplashSceneController : MonoBehaviour
{

    public KeyCode StartKey;

    private void Start()
    {
        MusicController.PlayMusic(Music.GENERAL);
    }

    private void Update()
    {
        if(Input.GetKeyDown(StartKey))
        {
            SoundEffectController.PlayerSoundEffect(SoundEffect.GAME_START);
            SceneSystem.FadeIn(TransitionType.FADE, StartGame, Color.black);
        }
    }

    private void StartGame()
    {
        SceneSystem.ChangeScene(Scene.GAME_SCENE);
        Time.timeScale = 0f;
        SceneSystem.FadeOut(TransitionType.FADE, () => Time.timeScale = 1f, Color.black);
    }

}
