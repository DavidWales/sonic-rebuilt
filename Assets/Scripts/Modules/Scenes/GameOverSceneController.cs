﻿using System.Collections;
using UnityEngine;

public class GameOverSceneController : MonoBehaviour
{

    public float WaitTime = 5.0f;
    public Scene DestinationScene = Scene.GAME_SCENE;
    public Color FadeColor = Color.black;
    public Music Music;
    public bool LoopMusic = true;

    private void Awake()
    {
        StartCoroutine(GameOverCycle());
    }

    private void Start()
    {
        MusicController.PlayMusic(Music, LoopMusic);
    }

    private IEnumerator GameOverCycle()
    {
        yield return new WaitForSeconds(WaitTime);
        SceneSystem.FadeIn(TransitionType.FADE, () =>
        {
            SceneSystem.ChangeScene(DestinationScene);
            Time.timeScale = 0f;
            SceneSystem.FadeOut(TransitionType.FADE, () => Time.timeScale = 1f, FadeColor);
        }, FadeColor);
    }

}
