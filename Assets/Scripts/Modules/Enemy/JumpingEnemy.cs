﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class JumpingEnemy : BaseEnemy
{

    public float MinimumJumpCooldown = 5f;
    public float MaximumJumpCooldown = 10f;
    public ApplyForceCommandParameter JumpCommandParameter;

    private ApplyForceCommand _jumpCommand;

    protected override void Awake()
    {
        base.Awake();

        JumpCommandParameter.Direction = Vector2.up;
        JumpCommandParameter.Rigidbody = GetComponent<Rigidbody2D>();
        _jumpCommand = new ApplyForceCommand(JumpCommandParameter);
    }

    public override void OnAggro()
    {
        base.OnAggro();
        StartCoroutine(JumpCycle());
    }

    private IEnumerator JumpCycle()
    {
        while (true)
        {
            _jumpCommand.Execute();
            var cooldown = Random.Range(MinimumJumpCooldown, MaximumJumpCooldown);
            yield return new WaitForSeconds(cooldown);
        }
    }

    private void OnDestroy()
    {
        Destroy(transform.parent.gameObject);
    }

}
