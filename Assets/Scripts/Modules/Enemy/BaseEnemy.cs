﻿using Talos.Common.DesignPatterns.Observer;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HealthComponent))]
public abstract class BaseEnemy : MonoBehaviour, IObserver<HealthComponent>
{

    public ExplodeCommandParameter ExplodeCommandParameter;
    public DamageCommandParameter PlayerCollisionDamageCommandParameter;
    public bool SelfDestructOnPlayerCollision = true;
    public bool DestroyAfterOffScreen = true;
    public string TargetTag = "Player";
    public float AggroRange = 10f;
    public SoundEffect DeathSoundEffect = SoundEffect.NONE;

    protected bool IsAggro = false;
    protected GameObject Player;

    private ExplodeCommand _explodeCommand;
    private DamageCommand _playerCollisionDamageCommand;
    private DamageCommand _selfDestructDamageCommand;
    private bool _wasVisible = false;
    private bool _isDestroying = false;

    protected virtual void Awake()
    {
        ExplodeCommandParameter.Source = gameObject;
        _explodeCommand = new ExplodeCommand(ExplodeCommandParameter);

        PlayerCollisionDamageCommandParameter.Source = gameObject;
        _playerCollisionDamageCommand = new DamageCommand(PlayerCollisionDamageCommandParameter);

        var healthComponent = GetComponent<HealthComponent>();

        _selfDestructDamageCommand = new DamageCommand(new DamageCommandParameter
        {
            Source = gameObject,
            Target = gameObject,
            Damage = healthComponent.MaxHealth
        });

        Player = GameObject.FindGameObjectWithTag(TargetTag);
        healthComponent.AttachObserver(this);
    }

    protected virtual void Update()
    {
        CheckAggro();
    }

    protected virtual void CheckAggro()
    {
        if (Player == null || IsAggro) return;

        var distance = Vector2.Distance(transform.position, Player.transform.position);
        if (distance > AggroRange) return;

        OnAggro();
    }

    public virtual void OnAggro()
    {
        IsAggro = true;
    }

    private void Explode()
    {
        if (_isDestroying) return;
        _isDestroying = true;
        _explodeCommand.Execute();
        IsAggro = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.TryGetComponent(out PlayerController player))
        {
            PlayerCollisionDamageCommandParameter.Target = player.gameObject;
            _playerCollisionDamageCommand.Execute();
            if(SelfDestructOnPlayerCollision) _selfDestructDamageCommand.Execute();
        }
    }

    public void Handle(HealthComponent subject)
    {
        if (_isDestroying) return;
        if (subject.CurrentHealth > 0)
        {
            SoundEffectController.PlayerSoundEffect(SoundEffect.ENEMY_HURT);
            return;
        }
        SoundEffectController.PlayerSoundEffect(DeathSoundEffect);
        Explode();
    }

    private void OnBecameVisible()
    {
        _wasVisible = true;
    }

    private void OnBecameInvisible()
    {
        if(_wasVisible && DestroyAfterOffScreen)
        {
            _selfDestructDamageCommand.Execute();
        }
    }

}
