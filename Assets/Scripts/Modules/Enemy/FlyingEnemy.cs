﻿using UnityEngine;

public class FlyingEnemy : BaseEnemy
{

    public AddVelocityCommandParameter MoveCommandParameter;
    public AddVelocityCommandParameter OscilateParameter;
    public float MaximumOscilation = 5f;

    private AddVelocityCommand _moveCommand;
    private AddVelocityCommand _oscilateCommand;
    private float _initialVerticalPosition;
    private Rigidbody2D _rigidBody;

    protected override void Awake()
    {
        base.Awake();

        _rigidBody = GetComponent<Rigidbody2D>();

        MoveCommandParameter.Direction = Vector2.left;
        MoveCommandParameter.Rigidbody = _rigidBody;
        _moveCommand = new AddVelocityCommand(MoveCommandParameter);

        OscilateParameter.Direction = Vector2.up;
        OscilateParameter.Rigidbody = _rigidBody;
        _oscilateCommand = new AddVelocityCommand(OscilateParameter);
    }

    protected override void Update()
    {
        base.Update();

        if (!IsAggro) return;
        Stop();
        _oscilateCommand.Execute();
        _moveCommand.Execute();
        CheckOscilation();
    }

    private void Stop()
    {
        _rigidBody.velocity = Vector2.zero;
    }

    public override void OnAggro()
    {
        base.OnAggro();
        _initialVerticalPosition = transform.position.y;
    }

    private void CheckOscilation()
    {
        var offset = transform.position.y - _initialVerticalPosition;
        if (Mathf.Abs(offset) > MaximumOscilation)
        {
            OscilateParameter.Direction = offset > 0
                ? Vector2.down
                : Vector2.up;
        }
    }

}
