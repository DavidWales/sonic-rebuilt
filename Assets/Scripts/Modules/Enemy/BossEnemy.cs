﻿using UnityEngine;

[RequireComponent(typeof(ChainAbilityCaster))]
public class BossEnemy : BaseEnemy
{

    private ChainAbilityCaster _chainAbilityCaster;
    private float _xCoord;

    protected override void Awake()
    {
        base.Awake();

        _chainAbilityCaster = GetComponent<ChainAbilityCaster>();
        _chainAbilityCaster.Target = Player;
        _chainAbilityCaster.SetActive(IsAggro);

        var healthBar = GameObject.FindGameObjectWithTag("EnemyHealthBar");
        EnableHealthBar(healthBar);

        _xCoord = GetMiddleScreenCoordinate();
    }

    protected override void Update()
    {
        base.Update();

        if (!IsAggro) return;
        FacePlayer();
        ClampPositionToRightHalf();
    }

    public override void OnAggro()
    {
        base.OnAggro();
        _chainAbilityCaster.SetActive(IsAggro);
    }

    private void FacePlayer()
    {
        if (Player == null) return;
        var dir = Player.transform.position.x > transform.position.x
            ? Vector2.right
            : Vector2.left;
        transform.localScale = new Vector2(dir.x * Mathf.Abs(transform.localScale.x), transform.localScale.y);
    }

    private void EnableHealthBar(GameObject healthBar)
    {
        if (healthBar == null) return;
        var healthTracker = healthBar.GetComponent<DynamicHealthTracker>();
        healthTracker.RegisterTrackedObject(gameObject);
    }

    private float GetMiddleScreenCoordinate()
    {
        var middle = new Vector2(Screen.width * 0.75f, Screen.height * 0.75f);
        return Camera.main.ScreenToWorldPoint(middle).x;
    }

    private void ClampPositionToRightHalf()
    {
        if(transform.position.x < _xCoord)
        {
            var distance = _xCoord - transform.position.x;
            transform.position += (Vector3.right * distance);
        }
    }

}
