﻿using System;
using UnityEngine;

[Serializable]
public class EnemyPositionEntity
{

    public float x;

    public float y;

    public float z = 0f;

    public Vector3 PositionVector => new Vector3(x, y, z);

}
