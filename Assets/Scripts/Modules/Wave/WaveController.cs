﻿using System.Collections;
using System.Collections.Generic;
using Talos.Common.DesignPatterns.Observer;
using UnityEngine;
using Newtonsoft.Json;
using System.Linq;

public class WaveController : SingletonBehavior<WaveController>, IObserver<WaveEntity>
{

    public List<WaveEntity> Waves;
    public string CharacterSpriteDirectory;
    public string ClipDirectory;
    public string WaveDirectory;
    public List<string> WaveFileNames;
    public List<EnemyTypeMapping> EnemyTypeMappings;
    public Vector3 SpawnOffset;

    protected override void Awake()
    {
        base.Awake();
        ReadWaves();
        StartCoroutine(StartNextWave());
    }

    private void ReadWaves()
    {
        foreach(var file in WaveFileNames)
        {
            var path = $"{WaveDirectory}/{file}";
            var waveJson = Resources.Load<TextAsset>(path);
            var wave = JsonConvert.DeserializeObject<WaveEntity>(waveJson.text);
            Waves.Add(wave);
        }
    }

    private IEnumerator StartNextWave()
    {
        if (Waves.Count <= 0) yield break;
        var nextWave = Waves[0];
        QueueDialogue(nextWave);
        yield return new WaitForSeconds(nextWave.WaveDelay ?? 0f);
        nextWave.AttachObserver(this);
        nextWave.SpawnEnemies(this);
    }

    public GameObject GetPrefabFromName(string name)
    {
        return EnemyTypeMappings.FirstOrDefault(etm => etm.Name == name)?.Prefab;
    }

    public void Handle(WaveEntity subject)
    {
        Waves.Remove(subject);
        if (!gameObject.activeInHierarchy) return;
        StartCoroutine(StartNextWave());
    }

    private void QueueDialogue(WaveEntity wave)
    {
        if (wave.Dialogue == null) return;
        foreach (var d in wave.Dialogue)
        {
            DialogueController.QueueDialogue(new DialogueModel
            {
                CharacterSprite = LoadResource<Sprite>(CharacterSpriteDirectory, d.CharacterSpriteFile),
                Text = d.Text,
                Clip = LoadResource<AudioClip>(ClipDirectory, d.ClipFile)
            });
        }
    }

    private T LoadResource<T>(string directory, string file) where T : UnityEngine.Object
    {
        if (string.IsNullOrWhiteSpace(file)) return default;
        var path = $"{directory}/{file}";
        return Resources.Load<T>(path);
    }

}
