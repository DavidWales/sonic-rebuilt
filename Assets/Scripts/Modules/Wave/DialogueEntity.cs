﻿using System;

[Serializable]
public class DialogueEntity
{

    public string Text;

    public string CharacterSpriteFile;

    public string ClipFile = null;

}
