﻿using System;
using System.Collections;
using Talos.Common.DesignPatterns.Observer;
using UnityEngine;

[Serializable]
public class EnemyEntity : BaseObserverSubject<EnemyEntity>, Talos.Common.DesignPatterns.Observer.IObserver<HealthComponent>
{

    public string Type;
    public EnemyPositionEntity Position;
    public float PositioningSpeed = 10f;
    public float SpawnDelay = 0f;

    private readonly float _distanceThreshold = 1f;

    public void SpawnEnemy(WaveController waveController)
    {
        var enemyPrefab = waveController.GetPrefabFromName(Type);
        var enemy = GameObject.Instantiate(
                enemyPrefab,
                Position.PositionVector + waveController.SpawnOffset,
                Quaternion.identity,
                waveController.transform);

        var healthComponent = enemy.GetComponentInChildren<HealthComponent>();
        if (healthComponent == null) return;
        healthComponent.AttachObserver(this);

        waveController.StartCoroutine(MoveToPosition(enemy));
    }

    public void Handle(HealthComponent subject)
    {
        if (subject.CurrentHealth > 0f) return;
        NotifyObservers();
    }

    private IEnumerator MoveToPosition(GameObject enemy)
    {
        SetRigidbodyActive(enemy, isActive: false);
        while (Vector3.Distance(enemy.transform.position, Position.PositionVector) > _distanceThreshold)
        {
            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, Position.PositionVector, PositioningSpeed * Time.deltaTime);
            yield return null;
        }
        SetRigidbodyActive(enemy, isActive: true);
        SetEnemyAggroed(enemy);
    }

    private void SetRigidbodyActive(GameObject enemy, bool isActive)
    {
        var rigidBodies = enemy.GetComponentsInChildren<Rigidbody2D>();
        foreach(var rigidBody in rigidBodies)
        {
            rigidBody.simulated = isActive;
        }
    }

    private void SetEnemyAggroed(GameObject enemy)
    {
        var e = enemy.GetComponent<BaseEnemy>();
        if (e == null) return;
        e.OnAggro();
    }

}
