﻿using System;
using UnityEngine;

[Serializable]
public class EnemyTypeMapping
{

    public string Name;

    public GameObject Prefab;

}
