﻿using System;
using System.Collections;
using System.Collections.Generic;
using Talos.Common.DesignPatterns.Observer;
using UnityEngine;

[Serializable]
public class WaveEntity : BaseObserverSubject<WaveEntity>, Talos.Common.DesignPatterns.Observer.IObserver<EnemyEntity>
{

    public List<DialogueEntity> Dialogue;
    public List<EnemyEntity> Enemies;
    public float? WaveDelay;

    private bool _waveComplete = false;

    public void SpawnEnemies(WaveController waveController)
    {
        foreach(var e in Enemies)
        {
            waveController.StartCoroutine(SpawnEnemy(waveController, e));
        }
    }

    private IEnumerator SpawnEnemy(WaveController waveController, EnemyEntity enemy)
    {
        yield return new WaitForSeconds(enemy.SpawnDelay);
        enemy.SpawnEnemy(waveController);
        enemy.AttachObserver(this);
    }

    public void Handle(EnemyEntity subject)
    {
        if (_waveComplete) return;

        Enemies.Remove(subject);
        if (Enemies.Count > 0) return;

        _waveComplete = true;
        NotifyObservers();
    }
}
