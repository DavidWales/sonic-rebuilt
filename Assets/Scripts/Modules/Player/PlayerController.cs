﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(HealthComponent))]
public class PlayerController : MonoBehaviour, Talos.Common.DesignPatterns.Observer.IObserver<HealthComponent>
{
    public AddVelocityCommandParameter MoveCommandParameter;
    public ShootCommandParameter ShootCommandParameter;
    public ExplodeCommandParameter ExplodeCommandParameter;
    public InvincibilityCommandParameter InvincibilityCommandParameter;

    private AddVelocityCommand _moveCommand;
    private ShootCommand _shootCommand;
    private ExplodeCommand _explodeCommand;
    private InvincibilityCommand _invincibilityCommand;
    private Dictionary<Func<bool>, Action> _movementActions;
    private Rigidbody2D _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();

        MoveCommandParameter.Rigidbody = _rigidBody;
        _moveCommand = new AddVelocityCommand(MoveCommandParameter);

        ShootCommandParameter.Source = gameObject;
        _shootCommand = new ShootCommand(ShootCommandParameter);

        ExplodeCommandParameter.Source = gameObject;
        _explodeCommand = new ExplodeCommand(ExplodeCommandParameter);

        InvincibilityCommandParameter.Source = gameObject;
        _invincibilityCommand = new InvincibilityCommand(InvincibilityCommandParameter);

        _movementActions = new Dictionary<Func<bool>, Action>
        {
            { () => Input.GetKey(KeyCode.W), () => Move(Vector2.up) },
            { () => Input.GetKey(KeyCode.A), () => Move(Vector2.left) },
            { () => Input.GetKey(KeyCode.S), () => Move(Vector2.down) },
            { () => Input.GetKey(KeyCode.D), () => Move(Vector2.right) },
            { () => Input.GetKey(KeyCode.UpArrow), () => Move(Vector2.up) },
            { () => Input.GetKey(KeyCode.LeftArrow), () => Move(Vector2.left) },
            { () => Input.GetKey(KeyCode.DownArrow), () => Move(Vector2.down) },
            { () => Input.GetKey(KeyCode.RightArrow), () => Move(Vector2.right) },
            { () => Input.GetKeyDown(KeyCode.Space), () => _shootCommand.Execute() }
        };

        var healthComponent = GetComponent<HealthComponent>();
        healthComponent.AttachObserver(this);
    }

    public void Update()
    {
        Stop();
        foreach(var action in _movementActions)
        {
            if (action.Key()) action.Value();
        }
    }

    private void Move(Vector2 direction)
    {
        MoveCommandParameter.Direction = direction;
        _moveCommand.Execute();
    }

    private void Stop()
    {
        _rigidBody.velocity = Vector2.zero;
    }

    public void Handle(HealthComponent subject)
    {
        _invincibilityCommand.Execute();
        if (subject.CurrentHealth > 0)
        {
            SoundEffectController.PlayerSoundEffect(SoundEffect.PLAYER_HURT);
            return;
        }

        SoundEffectController.PlayerSoundEffect(SoundEffect.PLAYER_DEATH);
        SoundEffectController.PlayerSoundEffect(SoundEffect.LOSE);
        Destroy(gameObject);
        _explodeCommand.Execute();
        LevelController.LevelFailed();
    }

}
