﻿using System;
using UnityEngine;

[Serializable]
public class DialogueModel
{

    public Sprite CharacterSprite;

    public string Text;

    public AudioClip Clip = null;

}