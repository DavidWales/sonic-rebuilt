﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class DialogueController : SingletonBehavior<DialogueController>
{

    public Image CharacterImage;
    public Text DialogueText;
    public float CharacterDelay = 0.1f;
    public float PauseSeconds = 5f;

    private Queue<DialogueModel> _dialogueQueue;
    private Coroutine _currentCoroutine;
    private AudioSource _audioSource;
    private string _nonBreakingSpace = "\u00A0";

    protected override void Awake()
    {
        base.Awake();
        _dialogueQueue = new Queue<DialogueModel>();
        _audioSource = GetComponent<AudioSource>();
        gameObject.SetActive(false);
    }

    public static void QueueDialogue(DialogueModel dialogue)
    {
        var instance = GetInstance();
        if(instance._currentCoroutine != null)
        {
            instance._dialogueQueue.Enqueue(dialogue);
            return;
        }
        instance.DisplayDialogue(dialogue);
    }

    private void DisplayDialogue(DialogueModel dialogue)
    {
        gameObject.SetActive(true);
        if(dialogue.Clip != null) _audioSource.PlayOneShot(dialogue.Clip);
        CharacterImage.sprite = dialogue.CharacterSprite;
        DialogueText.text = GeneratePlaceholderText(dialogue.Text);
        _currentCoroutine = StartCoroutine(WriteText(dialogue.Text));
    }

    private IEnumerator WriteText(string text)
    {
        var displayedText = new StringBuilder(DialogueText.text);
        for(int i = 0; i < text.Length; i++)
        {
            if(text[i] != ' ') yield return new WaitForSecondsRealtime(CharacterDelay);
            displayedText[i] = text[i];
            DialogueText.text = displayedText.ToString();
        }
        StartCoroutine(TimeoutDialogue());
    }

    private IEnumerator TimeoutDialogue()
    {
        yield return new WaitForSeconds(PauseSeconds);
        _currentCoroutine = null;
        if(_dialogueQueue.Count > 0)
        {
            //Display next dialogue
            var dialogue = _dialogueQueue.Dequeue();
            DisplayDialogue(dialogue);
        }
        else
        {
            //No other dialogues to display
            gameObject.SetActive(false);
        }
    }

    private string GeneratePlaceholderText(string text)
    {
        string result = string.Empty;
        foreach(var c in text)
        {
            if(char.IsWhiteSpace(c))
            {
                result += c;
                continue;
            }
            result += _nonBreakingSpace;
        }
        return result;
    }

}
