﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolWarmup : MonoBehaviour
{

    [Serializable]
    public struct WarmupDefinition
    {
        public GameObject Obj;
        public int Count;
    }

    public List<WarmupDefinition> Warmup;


    private void Start()
    {
        foreach(var x in Warmup)
        {
            ObjectPoolManager.Warmup(x.Count, x.Obj);
        }
    }
}
