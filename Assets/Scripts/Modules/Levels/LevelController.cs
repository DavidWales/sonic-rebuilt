﻿using System.Collections.Generic;
using UnityEngine;

public class LevelController : SingletonBehavior<LevelController>
{

    public Color VictoryFadeColor = Color.white;
    public Color DefeatFadeColor = Color.black;
    public List<DialogueModel> LevelStartDialogue;

    private bool _complete = false;

    private void Start()
    {
        MusicController.PlayMusic(Music.GENERAL);
        QueueDialogue();
    }

    public static void LevelComplete()
    {
        var instance = GetInstance();
        instance._complete = true;
        instance.Victory();
    }

    public static void LevelFailed()
    {
        var instance = GetInstance();
        if (instance._complete) return;
        instance.Defeat();
    }

    private void QueueDialogue()
    {
        if (LevelStartDialogue == null) return;
        foreach (var dialogue in LevelStartDialogue)
        {
            DialogueController.QueueDialogue(dialogue);
        }
    }

    private void Victory()
    {
        SceneSystem.FadeIn(TransitionType.FADE, () =>
        {
            SceneSystem.ChangeScene(Scene.WIN_SCENE);
            SceneSystem.FadeOut(TransitionType.FADE, () => { }, VictoryFadeColor);
        }, VictoryFadeColor);
    }

    private void Defeat()
    {
        SceneSystem.FadeIn(TransitionType.FADE, () =>
        {
            SceneSystem.ChangeScene(Scene.LOSS_SCENE);
            SceneSystem.FadeOut(TransitionType.FADE, () => { }, DefeatFadeColor);
        }, DefeatFadeColor);
    }

}
