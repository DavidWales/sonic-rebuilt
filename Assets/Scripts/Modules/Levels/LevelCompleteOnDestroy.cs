﻿using Talos.Common.DesignPatterns.Observer;
using UnityEngine;

[RequireComponent(typeof(HealthComponent))]
public class LevelCompleteOnDestroy : MonoBehaviour, IObserver<HealthComponent>
{

    private bool _markedComplete = false;

    private void Awake()
    {
        var healthComponent = GetComponent<HealthComponent>();
        healthComponent.AttachObserver(this);
    }

    public void Handle(HealthComponent subject)
    {
        if (subject.CurrentHealth > 0 || _markedComplete) return;
        _markedComplete = true;
        SoundEffectController.PlayerSoundEffect(SoundEffect.WIN);
        LevelController.LevelComplete();
    }

}
