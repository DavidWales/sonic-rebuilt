﻿using Talos.Common.DesignPatterns.Command;

public class ApplyForceCommand : BaseCommand<ApplyForceCommandParameter>
{

    public ApplyForceCommand(ApplyForceCommandParameter parameter)
        : base(parameter)
    { }

    public override void Execute()
    {
        Parameter.Rigidbody.AddForce(Parameter.Direction * Parameter.Force);
    }

}
