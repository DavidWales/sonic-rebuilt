﻿using System.Collections;
using System.Collections.Generic;
using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class BulletConeCommand : BaseCommand<BulletConeCommandParameter>
{

    private List<ShootCommand> _shootCommands;
    private MonoBehaviour _behaviour;

    public BulletConeCommand(BulletConeCommandParameter parameter)
        : base(parameter)
    {
        InitializeShootCommands();
        _behaviour = Parameter.Source.GetComponent<MonoBehaviour>();
    }

    public override void Execute()
    {
        if (_behaviour == null) return;
        _behaviour.StartCoroutine(Shoot());
    }

    private IEnumerator Shoot()
    {
        var halfAngle = Parameter.Angle / 2f;
        foreach (var shot in _shootCommands)
        {
            yield return new WaitForSeconds(Parameter.Delay);
            var rotation = Quaternion.Euler(0f, 0f, Random.Range(-halfAngle, halfAngle));
            var dir = rotation * Parameter.Direction;
            shot.Parameter.Direction = dir.normalized;
            shot.Execute();
        }
    }

    private void InitializeShootCommands()
    {
        _shootCommands = new List<ShootCommand>();
        for(int i = 0; i < Parameter.NumBullets; i++)
        {
            _shootCommands.Add(new ShootCommand(new ShootCommandParameter
            {
                Speed = Parameter.BulletSpeed,
                Direction = Vector2.right, //To be dynamic set later
                BulletPrefab = Parameter.BulletPrefab,
                SpawnPoint = Parameter.SpawnPoint,
                Source = Parameter.Source,
                SoundEffect = Parameter.SoundEffect
            }));
        }
    }

}
