﻿using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class ShootCommand : BaseCommand<ShootCommandParameter>
{

    public ShootCommand(ShootCommandParameter parameter)
        : base(parameter)
    {
        
    }

    public override void Execute()
    {
        Parameter.SpawnPoint = Parameter.SpawnPoint ?? Parameter.Source;
        var bullet = ObjectPoolManager.Instantiate(Parameter.BulletPrefab, Parameter.SpawnPoint.transform.position, 
            Quaternion.identity);

        if (!bullet.TryGetComponent(out Bullet bulletComponent)) return;
        if (!bullet.TryGetComponent(out Rigidbody2D bulletRigidbody)) return;

        var dir = GetDirection();
        bulletRigidbody.velocity = dir * Parameter.Speed;
        bullet.transform.right = dir;
        bulletComponent.Initialize(Parameter.Source, Parameter.InitializationParameters);

        SoundEffectController.PlayerSoundEffect(Parameter.SoundEffect);
    }

    private Vector2 GetDirection()
    {
        if (Parameter.Direction.HasValue) return Parameter.Direction.Value;
        var facingDir = Parameter.Source.transform.localScale.x >= 0 ? 1 : -1;
        if (Parameter.FlipScaleDirection) facingDir *= -1;
        return Vector2.right * facingDir;
    }

}