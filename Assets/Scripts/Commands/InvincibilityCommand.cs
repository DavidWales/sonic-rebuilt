﻿using System.Collections;
using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class InvincibilityCommand : BaseCommand<InvincibilityCommandParameter>
{

    public InvincibilityCommand(InvincibilityCommandParameter parameter)
        : base(parameter)
    { }

    public override void Execute()
    {
        if (!Parameter.Source.activeInHierarchy) return;

        var healthComponent = Parameter.Source.GetComponent<HealthComponent>();
        if (healthComponent == null) return;

        healthComponent.StartCoroutine(AlphaFlash(healthComponent));
    }

    private IEnumerator AlphaFlash(HealthComponent healthComponent)
    {
        var spriteRenderer = Parameter.Source.GetComponent<SpriteRenderer>();
        if (spriteRenderer == null) yield break;
        healthComponent.Invincible = true;
        var initialAlpha = spriteRenderer.color.a;
        var color = spriteRenderer.color;

        float time = 0f;
        int dir = -1;
        while(time < Parameter.Duration)
        {
            color.a += dir * Parameter.AlphaSpeed * Time.deltaTime;
            color.a = Mathf.Clamp(color.a, Parameter.AlphaRange.x, Parameter.AlphaRange.y);
            spriteRenderer.color = color;
            if (color.a <= Parameter.AlphaRange.x || color.a >= Parameter.AlphaRange.y)
            {
                dir *= -1;
            }

            yield return null;
            time += Time.deltaTime;
        }

        color.a = initialAlpha;
        spriteRenderer.color = color;
        healthComponent.Invincible = false;
    }

}
