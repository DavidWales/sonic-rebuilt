﻿using System;
using System.Collections;
using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class ExplodeCommand : BaseCommand<ExplodeCommandParameter>
{

    public ExplodeCommand(ExplodeCommandParameter parameter)
        : base(parameter)
    {
        
    }

    public override void Execute()
    {
        if (!Parameter.Source.activeInHierarchy) return;

        var mono = Parameter.Source.GetComponent<MonoBehaviour>();
        if (mono == null) return;

        mono.StartCoroutine(ShrinkAway());
    }

    private IEnumerator ShrinkAway()
    {
        while(Vector3.Distance(Parameter.Source.transform.localScale, Vector3.zero) > 1e-3)
        {
            yield return null;
            Parameter.Source.transform.localScale = Vector3.MoveTowards(
                Parameter.Source.transform.localScale, 
                Vector3.zero, 
                Parameter.ShrinkSpeed * Time.deltaTime);
        }

        if (Parameter.Recycle) ObjectPoolManager.Destroy(Parameter.Source);
        else GameObject.Destroy(Parameter.Source);
    }

}