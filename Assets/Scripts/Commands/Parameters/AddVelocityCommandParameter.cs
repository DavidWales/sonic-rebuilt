﻿using System;
using UnityEngine;

[Serializable]
public class AddVelocityCommandParameter
{

    public float Speed;

    [ReadOnly]
    public Rigidbody2D Rigidbody;

    [ReadOnly]
    public Vector2 Direction;

    public bool ManageScale = true;

}