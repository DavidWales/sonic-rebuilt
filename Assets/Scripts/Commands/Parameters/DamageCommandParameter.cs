﻿using System;
using UnityEngine;

[Serializable]
public class DamageCommandParameter
{

    public int Damage;

    [ReadOnly]
    public GameObject Source;

    [ReadOnly]
    public GameObject Target;

}
