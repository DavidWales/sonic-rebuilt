﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ShootCommandParameter
{

    public float Speed;

    public GameObject BulletPrefab;

    public GameObject SpawnPoint;

    public SoundEffect SoundEffect = SoundEffect.NONE;

    public bool FlipScaleDirection = false;

    [ReadOnly]
    public GameObject Source;

    [ReadOnly]
    public Vector2? Direction = null;

    [ReadOnly]
    public Dictionary<string, object> InitializationParameters;

}