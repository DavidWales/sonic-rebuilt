﻿using System;
using UnityEngine;

[Serializable]
public class ApplyForceCommandParameter
{

    public float Force;

    [ReadOnly]
    public Rigidbody2D Rigidbody;

    [ReadOnly]
    public Vector2 Direction;

}
