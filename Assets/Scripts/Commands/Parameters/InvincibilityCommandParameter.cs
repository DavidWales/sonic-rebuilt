﻿using System;
using UnityEngine;

[Serializable]
public class InvincibilityCommandParameter
{

    [ReadOnly]
    public GameObject Source;

    public float Duration = 3f;

    public Vector2 AlphaRange = new Vector2(0.5f, 1.0f);

    public float AlphaSpeed = 1f;

}
