﻿using System;
using UnityEngine;

[Serializable]
public class BulletExplosionCommandParameter
{

    public int NumBullets;

    public float BulletSpeed;

    public GameObject BulletPrefab;

    public SoundEffect SoundEffect = SoundEffect.NONE;

    [ReadOnly]
    public GameObject Source;

}
