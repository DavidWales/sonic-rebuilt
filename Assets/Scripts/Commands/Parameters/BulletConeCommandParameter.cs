﻿using System;
using UnityEngine;

[Serializable]
public class BulletConeCommandParameter
{

    public int NumBullets;

    public float BulletSpeed;

    public float Delay;

    public Vector2 Direction;

    public float Angle;

    public GameObject BulletPrefab;

    public GameObject SpawnPoint;

    public SoundEffect SoundEffect = SoundEffect.NONE;

    [ReadOnly]
    public GameObject Source;

}