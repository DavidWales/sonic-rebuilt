﻿using System;
using UnityEngine;

[Serializable]
public class ExplodeCommandParameter
{

    public float ShrinkSpeed = 1f;

    public bool Recycle = false;

    [ReadOnly]
    public GameObject Source;

}
