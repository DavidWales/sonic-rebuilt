﻿using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class DamageCommand : BaseCommand<DamageCommandParameter>
{

    public DamageCommand(DamageCommandParameter parameter)
        : base(parameter)
    { }

    public override void Execute()
    {
        if (Parameter.Target == null) return;

        var targetHealth = Parameter.Target.GetComponent<HealthComponent>();
        if (targetHealth == null || targetHealth.Invincible) return;

        targetHealth.CurrentHealth -= Parameter.Damage;
        targetHealth.CurrentHealth = Mathf.Clamp(targetHealth.CurrentHealth, 0, targetHealth.MaxHealth);
        targetHealth.NotifyObservers();
    }

}
