﻿using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class AddVelocityCommand : BaseCommand<AddVelocityCommandParameter>
{

    public AddVelocityCommand(AddVelocityCommandParameter parameter)
        : base(parameter)
    { }

    public override void Execute()
    {
        Parameter.Rigidbody.velocity += Parameter.Direction * Parameter.Speed;

        if (Parameter.ManageScale && Parameter.Rigidbody.velocity.x != 0f)
        {
            var dirSign = Parameter.Rigidbody.velocity.x != 0f
                ? Parameter.Rigidbody.velocity.x / Mathf.Abs(Parameter.Rigidbody.velocity.x)
                : 1;
            var scale = Parameter.Rigidbody.transform.localScale;
            Parameter.Rigidbody.transform.localScale = new Vector3(dirSign * Mathf.Abs(scale.x), scale.y, scale.z);
        }
    }

}
