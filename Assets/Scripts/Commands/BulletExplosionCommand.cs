﻿using System.Collections.Generic;
using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public class BulletExplosionCommand : BaseCommand<BulletExplosionCommandParameter>
{

    private List<ShootCommand> _bulletRainCommands;

    public BulletExplosionCommand(BulletExplosionCommandParameter parameter)
        : base(parameter)
    {
        InitializeShootCommands();
    }

    public override void Execute()
    {
        foreach (var shootCommand in _bulletRainCommands)
        {
            shootCommand.Parameter.Speed = Parameter.BulletSpeed;
            shootCommand.Execute();
        }
        SoundEffectController.PlayerSoundEffect(Parameter.SoundEffect);
    }

    private void InitializeShootCommands()
    {
        _bulletRainCommands = new List<ShootCommand>();
        var angleIncrement = 360f / Parameter.NumBullets;
        for (int i = 0; i < Parameter.NumBullets; i++)
        {
            var dir = Quaternion.AngleAxis(i * angleIncrement, Vector3.forward) * Vector3.up;
            _bulletRainCommands.Add(new ShootCommand(new ShootCommandParameter
            {
                BulletPrefab = Parameter.BulletPrefab,
                Source = Parameter.Source,
                Direction = dir,
                Speed = Parameter.BulletSpeed
            }));
        }
    }

}