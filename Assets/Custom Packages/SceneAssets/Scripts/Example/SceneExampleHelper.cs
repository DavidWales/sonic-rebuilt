﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneExampleHelper : MonoBehaviour
{
    public Scene nextScene;

    public void NextSceneButtonClick()
    {
        SceneSystem.FadeIn(TransitionType.FADE, () =>
        {
            SceneSystem.ChangeScene(nextScene);
            SceneSystem.FadeOut(TransitionType.FADE, () => { });
        }, Color.blue);
    }
}