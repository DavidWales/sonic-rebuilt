﻿using System;

[Serializable]
public class TransitionDefinition
{
    public TransitionType transitionType;
    public Transition transition;
}