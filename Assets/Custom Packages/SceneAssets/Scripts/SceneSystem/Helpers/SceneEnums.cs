﻿using System.ComponentModel;

/// <summary>
/// Scene enum to more easily reference scenes included in the build. Must replace
/// with real values if copied into project.
/// </summary>
public enum Scene
{

    [Description("Splash Scene")]
    SPLASH_SCENE = 0,

    [Description("Game Scene")]
    GAME_SCENE = 1,

    [Description("Loss Scene")]
    LOSS_SCENE = 2,

    [Description("Win Scene")]
    WIN_SCENE = 3
}

public enum TransitionType
{
    FADE = 0
}