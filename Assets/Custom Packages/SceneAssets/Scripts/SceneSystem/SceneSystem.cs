﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSystem : MonoBehaviour
{
    public List<TransitionDefinition> transitions;

    private bool _isTransitioning = false;

    #region Core Methods

    public static void FadeOut(TransitionType transitionType, Action callback, Color? color = null)
    {
        if (_inst._isTransitioning == false && _inst.transitions.Any(t => t.transitionType == transitionType))
        {
            _inst._isTransitioning = true;
            Transition currentTransition = _inst.transitions.FirstOrDefault(t => t.transitionType == transitionType).transition;
            _inst.SetTransitionColor(currentTransition, color);
            currentTransition.FadeOut(() => {
                _inst._isTransitioning = false;
                callback();
            });
        }
    }

    public static void FadeIn(TransitionType transitionType, Action callback, Color? color = null)
    {
        if (_inst._isTransitioning == false && _inst.transitions.Any(t => t.transitionType == transitionType))
        {
            _inst._isTransitioning = true;
            Transition currentTransition = _inst.transitions.FirstOrDefault(t => t.transitionType == transitionType).transition;
            _inst.SetTransitionColor(currentTransition, color);
            currentTransition.FadeIn(() => {
                _inst._isTransitioning = false;
                callback();
            });
        }
    }

    public static void ChangeScene(Scene scene)
    {
        SceneManager.LoadScene((int)scene);
    }

    #endregion

    #region Helper Methods

    private void SetTransitionColor(Transition transition, Color? color)
    {
        if (color.HasValue)
        {
            Color newColor = color.Value;
            newColor.a = transition.graphic.color.a;
            transition.graphic.color = newColor;
        }
    }

    #endregion

    #region Singleton Stuff

    private static SceneSystem _inst = null;

    protected virtual void Awake()
    {
        if(_inst != null)
        {
            DestroyImmediate(this.gameObject);
            return;
        }
        _inst = this;
        DontDestroyOnLoad(this.gameObject);
    }

    #endregion

}