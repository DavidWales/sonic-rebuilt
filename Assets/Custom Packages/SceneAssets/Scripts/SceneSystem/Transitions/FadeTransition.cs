﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Generic color fade in and fade out behavior
/// </summary>
public class FadeTransition : Transition
{
    public float fadeInTime = 2.0f;
    public float fadeOutTime = 2.0f;

    #region Fading Methods

    /// <summary>
    /// Start process of fading in the resource
    /// </summary>
    /// <param name="callback">Method to call after completely faded in</param>
    public override void FadeIn(Action callback)
    {
        StartCoroutine(FadeInCoroutine(callback));
    }

    /// <summary>
    /// Slowly fade in the resource over "fadeInTime" seconds
    /// </summary>
    /// <param name="callback">Method to call after completely faded in</param>
    /// <returns></returns>
    private IEnumerator FadeInCoroutine(Action callback)
    {
        Color c;
        while (graphic.color.a < 1.0f)
        {
            c = graphic.color;
            c.a += Time.unscaledDeltaTime * (1.0f / fadeInTime);
            graphic.color = c;
            yield return null;
        }

        c = graphic.color;
        c.a = 1.0f;
        graphic.color = c;

        callback();
    }

    /// <summary>
    /// Slowly decrease alpha until fully invisible
    /// </summary>
    /// <param name="callback">Method to call after completely faded out</param>
    public override void FadeOut(Action callback)
    {
        StartCoroutine(FadeOutCoroutine(callback));
    }

    /// <summary>
    /// Slowly fade out the resource over "fadeOutTime" seconds
    /// </summary>
    /// <param name="callback">Method to call after completely faded out</param>
    /// <returns></returns>
    private IEnumerator FadeOutCoroutine(Action callback)
    {
        Color c;
        while (graphic.color.a > 0.0f)
        {
            c = graphic.color;
            c.a -= Time.unscaledDeltaTime * (1.0f / fadeOutTime);
            graphic.color = c;
            yield return null;
        }

        c = graphic.color;
        c.a = 0.0f;
        graphic.color = c;

        callback();
    }

    #endregion

}
