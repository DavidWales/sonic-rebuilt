﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Graphic))]
public abstract class Transition : MonoBehaviour
{
    [HideInInspector]
    public Graphic graphic;

    protected virtual void Awake()
    {
        graphic = this.GetComponent<Graphic>();
    }

    public abstract void FadeIn(Action callback);
    public abstract void FadeOut(Action callback);
}