﻿using System.Collections.Generic;
using UnityEngine;
using Talos.Common.DesignPatterns.ObjectPool;

public class ObjectPoolManager : SingletonBehavior<ObjectPoolManager>, IObjectPoolManager<GameObject>
{

    private Dictionary<string, List<GameObject>> _objectPool;

    protected override void Awake()
    {
        base.Awake();
        _objectPool = new Dictionary<string, List<GameObject>>();
    }

    public static void Warmup(int count, GameObject prefab)
    {
        string prefabName = prefab.name;
        GetInstance().InitializeObjectPoolList(prefabName);
        Transform objectPoolContainer = GetInstance().transform.Find(GetInstance().GetObjectPoolName(prefabName));

        while(GetInstance()._objectPool[prefabName].Count < count)
        {
            var newObj = GameObject.Instantiate(prefab);
            newObj.SetActive(false);
            newObj.transform.SetParent(objectPoolContainer);
            newObj.name = prefabName;
            GetInstance()._objectPool[prefabName].Add(newObj);
        }
    }

    public static GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        string prefabName = prefab.name;

        GameObject obj = null;
        if(GetInstance()._objectPool[prefabName]?.Count > 0)
        {
            obj = GetInstance()._objectPool[prefabName][0];
            GetInstance()._objectPool[prefabName].RemoveAt(0);
            obj.transform.SetParent(null);
        }
        else
        {
            obj = GameObject.Instantiate(prefab);
            obj.name = prefabName;
        }

        obj.SetActive(true);
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        
        if(parent != null)
        {
            obj.transform.SetParent(parent);
        }

        
        return obj;
    }

    public static void Destroy(GameObject obj)
    {
        string objName = obj.name;

        GetInstance().InitializeObjectPoolList(objName);
        obj.SetActive(false);
        GetInstance()._objectPool[objName].Add(obj);

        Transform objectPoolContainer = GetInstance().transform.Find(GetInstance().GetObjectPoolName(objName));
        obj.transform.SetParent(objectPoolContainer);
    }

    private void InitializeObjectPoolList(string objName)
    {
        if (!_objectPool.ContainsKey(objName))
        {
            var objectPoolContainer = new GameObject(GetObjectPoolName(objName));
            objectPoolContainer.transform.SetParent(transform);
            _objectPool[objName] = new List<GameObject>();
        }
    }

    private string GetObjectPoolName(string objName)
    {
        return $"{objName}ObjectPool";
    }

}
