﻿using Talos.Common.DesignPatterns.Command;
using UnityEngine;

public abstract class CommandBehaviour : MonoBehaviour, ICommand
{

    public abstract void Execute();

}
