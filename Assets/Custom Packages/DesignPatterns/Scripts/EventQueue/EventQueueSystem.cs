﻿using System;
using System.Collections.Generic;

public class EventQueueSystem : SingletonBehavior<EventQueueSystem>
{

    private Queue<IEvent> _eventQueue = new Queue<IEvent>();
    private Func<bool> _eventTrigger = null;

    private void Update()
    {
        if(_eventTrigger != null && _eventTrigger())
        {
            IEvent nextEvent = _eventQueue.Dequeue();
            if(nextEvent != null)
            {
                nextEvent.OnTriggered();
            }
        }
    }

    public static void QueueEvent(IEvent newEvent)
    {
        GetInstance()._eventQueue.Enqueue(newEvent);
    }

}
