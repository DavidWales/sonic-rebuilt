﻿using Talos.Common.DesignPatterns.Visitor;
using UnityEngine;

public class VisitorElementBehaviour : MonoBehaviour, IVisitorElement
{

    public virtual void Accept(IVisitor visitor)
    {
        visitor.Visit(this);
    }

}