﻿using System.Collections.Generic;
using Talos.Common.DesignPatterns.Observer;
using UnityEngine;

public class ObserverSubjectBehaviour<T> : MonoBehaviour, IObserverSubject<T> where T : ObserverSubjectBehaviour<T>
{

    private List<IObserver<T>> _observers;

    public ObserverSubjectBehaviour()
    {
        _observers = new List<IObserver<T>>();
    }

    public virtual void AttachObserver(IObserver<T> observer)
    {
        if (!_observers.Contains(observer))
        {
            _observers.Add(observer);
        }
    }

    public virtual void DetachObserver(IObserver<T> observer)
    {
        if (_observers.Contains(observer))
        {
            _observers.Remove(observer);
        }
    }

    public virtual void NotifyObservers()
    {
        //Safe because of generic type constraint
        var subject = this as T;
        foreach (var observer in _observers)
        {
            observer?.Handle(subject);
        }
    }

}
