﻿using Talos.Common.DesignPatterns.Singleton;
using UnityEngine;

public abstract class SingletonBehavior<T> : MonoBehaviour, ISingleton<SingletonBehavior<T>> where T: MonoBehaviour
{

    private static T _instance;

    protected static T GetInstance()
    {
        if(_instance == null)
        {
            var gameObj = new GameObject(typeof(T).Name);
            var singletonComponent = gameObj.AddComponent(typeof(T));
            _instance = singletonComponent as T;
        }
        return _instance;
    }

    protected virtual void Awake()
    {
        if (_instance != null)
        {
            GameObject.Destroy(this.gameObject);
            return;
        }
        _instance = this as T;
    }

}
